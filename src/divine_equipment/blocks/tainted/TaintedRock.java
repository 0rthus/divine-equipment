package blocks.tainted;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import divine_equipment.Divine_Equipment;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;

/**
 * User: Orthus
 * Date: 10/1/13
 */
public class TaintedRock extends Block {
     public TaintedRock (int id, Material material)
     {
         super(id, material);
         setHardness(1.0F);
         setStepSound(Block.soundStoneFootstep);
         setUnlocalizedName("Tainted Stone");
         setCreativeTab(CreativeTabs.tabBlock);
         setTextureName("divine_equipment:Tainted_Stone");
     }
}
