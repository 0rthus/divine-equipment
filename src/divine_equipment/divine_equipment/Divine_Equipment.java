package divine_equipment; /**
 * Author: Orthus
 * Date: 9/29/13
 */

import common.Config;
import common.Divine_Achievements;
import common.Divine_Content;
import common.Special_Mentions;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import net.minecraftforge.common.Configuration;



@SuppressWarnings("ALL")
@Mod(modid = "divine_equipment", name = "Divine Equipment",version = "V0.3")
@NetworkMod(clientSideRequired = true, serverSideRequired = false, channels = { "DEquipment" })
public class Divine_Equipment {
    public static final String modid = "divine_equipment";


    @Instance("Divine_Equipment")
    public static Divine_Equipment INSTANCE;

    public static Config config;



    public Divine_Equipment()
   {
       mentions = new Special_Mentions();

   }
    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent fEvent)
    {
        config = new Config(new Configuration(fEvent.getSuggestedConfigurationFile()));
    }
    @Mod.EventHandler
    public void load(FMLInitializationEvent fEvent)
    {
        content = new Divine_Content();
        achivements = new Divine_Achievements();
    }
    public Divine_Content content;
    public Divine_Achievements achivements;
    public Special_Mentions mentions;
}