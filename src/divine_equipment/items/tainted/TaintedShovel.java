package items.tainted;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemSpade;

/**
 * User: Orthus
 * Date: 10/8/13
 */
public class TaintedShovel extends ItemSpade {
    public TaintedShovel(int par1, EnumToolMaterial par2EnumToolMaterial) {
        super(par1, par2EnumToolMaterial);
        setCreativeTab(CreativeTabs.tabTools);
    }
}
