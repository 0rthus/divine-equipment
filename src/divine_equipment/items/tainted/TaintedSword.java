package items.tainted;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemSword;

/**
 * User: Orthus
 * Date: 9/29/13
 */
public class TaintedSword extends ItemSword {
    public TaintedSword(int id, EnumToolMaterial material){
        super(id, material);
        setCreativeTab(CreativeTabs.tabCombat);

    }
}
