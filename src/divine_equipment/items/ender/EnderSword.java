package items.ender;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSword;

/**
 * User: Orthus
 * Date: 10/12/13
 */
public class EnderSword extends ItemSword {
    public EnderSword(int id, EnumToolMaterial material){
        super(id, material);
        setCreativeTab(CreativeTabs.tabCombat);
    }

}
