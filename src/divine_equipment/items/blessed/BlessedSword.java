package items.blessed;

import net.minecraft.creativetab.*;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemSword;

/**
 * User: Orthus
 * Date: 9/30/13
 */
public class BlessedSword extends ItemSword {
    public BlessedSword(int id, EnumToolMaterial material){
        super(id, material);
        setCreativeTab(CreativeTabs.tabCombat);
    }
}