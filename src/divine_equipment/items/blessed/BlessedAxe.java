package items.blessed;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemAxe;

/**
 * User: Orthus
 * Date: 10/8/13
 */
public class BlessedAxe extends ItemAxe {
    public BlessedAxe(int par1, EnumToolMaterial par2EnumToolMaterial) {
        super(par1, par2EnumToolMaterial);
        setCreativeTab(CreativeTabs.tabTools);
    }
}
