package items.blessed;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemHoe;

/**
 * User: Orthus
 * Date: 10/8/13
 */
public class BlessedHoe extends ItemHoe {
    public BlessedHoe(int par1, EnumToolMaterial par2EnumToolMaterial) {
        super(par1, par2EnumToolMaterial);
        setCreativeTab(CreativeTabs.tabTools);
    }
}
