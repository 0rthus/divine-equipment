package items.blessed;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemPickaxe;

/**
 * User: Orthus
 * Date: 9/30/13
 */
public class BlessedPickaxe extends ItemPickaxe {
    public BlessedPickaxe(int id, EnumToolMaterial material){
        super(id, material);
        setCreativeTab(CreativeTabs.tabTools);
    }
}