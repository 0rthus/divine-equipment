package items;

import divine_equipment.Divine_Equipment;
import net.minecraft.item.Item;
import net.minecraft.creativetab.CreativeTabs;
/**
 * User: Orthus
 * Date: 11/25/13
 */
public class Sigil extends Item
{
    public Sigil(int fID, String fName, String fTexture)
    {
        super(fID);

        setMaxStackSize(1);
        setCreativeTab(CreativeTabs.tabMisc);
        setTextureName(fName);

        this.setTextureName(Divine_Equipment.modid + ":" + fTexture);
    }
}
