package common;

import net.minecraftforge.common.Configuration;
/**
 * User: Orthus
 * Date: 11/25/13
 */
public class Config {


    // Common
    private int BaseBlockID = 500;
    private int BaseID = 1200;
    // Blocks
    public final int TaintedStone, TaintedWood, BlessedStone, BlessedWood;

    // Sigils
    public final int BlankSigil, TaintedSigil, BlessedSigil, EnderSigil, HiddenSigil;
    // Tainted
    public final int TaintedDiamondSword, TaintedGoldSword, TaintedIronSword, TaintedStoneSword, TaintedWoodSword, TaintedDiamondPickaxe, TaintedGoldPickaxe, TaintedIronPickaxe, TaintedStonePickaxe, TaintedWoodPickaxe, TaintedDiamondAxe, TaintedGoldAxe, TaintedIronAxe, TaintedStoneAxe, TaintedWoodAxe, TaintedDiamondShovel, TaintedGoldShovel, TaintedIronShovel, TaintedStoneShovel, TaintedWoodShovel, TaintedDiamondHoe, TaintedGoldHoe, TaintedIronHoe, TaintedStoneHoe, TaintedWoodHoe;
    // Blessed
    public final int BlessedDiamondSword, BlessedGoldSword, BlessedIronSword, BlessedStoneSword, BlessedWoodSword, BlessedDiamondPickaxe, BlessedGoldPickaxe, BlessedIronPickaxe, BlessedStonePickaxe, BlessedWoodPickaxe, BlessedDiamondAxe, BlessedGoldAxe, BlessedIronAxe, BlessedStoneAxe, BlessedWoodAxe, BlessedDiamondShovel, BlessedGoldShovel, BlessedIronShovel, BlessedStoneShovel, BlessedWoodShovel, BlessedDiamondHoe, BlessedGoldHoe, BlessedIronHoe, BlessedStoneHoe, BlessedWoodHoe;
    // Ender Items
    public final int EnderSword;

    public Config(Configuration config)
    {
        config.load();
        TaintedStone = config.get("tainted stone", "BlockID", BaseBlockID).getInt(BaseBlockID);
        TaintedWood = config.get("tainted wood", "BlockID", BaseBlockID+ 1).getInt(BaseBlockID+1);
        BlessedStone = config.get("blessed stone", "BlockID", BaseBlockID+2).getInt(BaseBlockID+2);
        BlessedWood = config.get("blessed wood", "BlockID", BaseBlockID+3).getInt(BaseBlockID+3);
        BlankSigil = config.get("Blank Sigil", "ItemID", BaseID+4).getInt(BaseID+4);
        TaintedSigil = config.get("Tainted Sigil", "ItemID", BaseID+5).getInt(BaseID+5);
        BlessedSigil = config.get("Blessed Sigil", "ItemID", BaseID+6).getInt(BaseID+6);
        EnderSigil = config.get("Ender Sigil", "ItemID", BaseID+7).getInt(BaseID+7);
        HiddenSigil = config.get("Hidden Sigil", "ItemID", BaseID+8).getInt(BaseID+8);

        TaintedDiamondSword = config.get("Tainted Diamond Sword", "ItemID", BaseID+9).getInt(BaseID+9);
        TaintedGoldSword = config.get("Tainted Gold Sword", "ItemID", BaseID+10).getInt(BaseID+10);
        TaintedIronSword = config.get("Tainted Iron Sword", "ItemID", BaseID+11).getInt(BaseID+11);
        TaintedStoneSword = config.get("Tainted Stone Sword", "ItemID", BaseID+12).getInt(BaseID+12);
        TaintedWoodSword = config.get("Tainted Wood Sword", "ItemID", BaseID+13).getInt(BaseID+13);
        TaintedDiamondPickaxe = config.get("Tainted Diamond Pickaxe", "ItemID", BaseID+14).getInt(BaseID+14);
        TaintedGoldPickaxe = config.get("Tainted Gold Pickaxe", "ItemID", BaseID+15).getInt(BaseID+15);
        TaintedIronPickaxe = config.get("Tainted Iron Pickaxe", "ItemID", BaseID+16).getInt(BaseID+16);
        TaintedStonePickaxe = config.get("Tainted Stone Pickaxe", "ItemID", BaseID+17).getInt(BaseID+17);
        TaintedWoodPickaxe = config.get("Tainted Wood Pickaxe", "ItemID", BaseID+18).getInt(BaseID+18);
        TaintedDiamondAxe = config.get("Tainted Diamond Axe", "ItemID", BaseID+19).getInt(BaseID+19);
        TaintedGoldAxe = config.get("Tainted Gold Axe", "ItemID", BaseID+20).getInt(BaseID+20);
        TaintedIronAxe = config.get("Tainted Iron Axe", "ItemID", BaseID+21).getInt(BaseID+21);
        TaintedStoneAxe = config.get("Tainted Stone Axe", "ItemID", BaseID+22).getInt(BaseID+22);
        TaintedWoodAxe = config.get("Tainted Wood Axe", "ItemID", BaseID+23).getInt(BaseID+23);
        TaintedDiamondShovel = config.get("Tainted Diamond Shovel", "ItemID", BaseID+24).getInt(BaseID+24);
        TaintedGoldShovel = config.get("Tainted Gold Shovel", "ItemID", BaseID+25).getInt(BaseID+25);
        TaintedIronShovel = config.get("Tainted Iron Shovel", "ItemID", BaseID+26).getInt(BaseID+26);
        TaintedStoneShovel = config.get("Tainted Stone Shovel", "ItemID", BaseID+27).getInt(BaseID+27);
        TaintedWoodShovel = config.get("Tainted Wood Shovel", "ItemID", BaseID+28).getInt(BaseID+28);
        TaintedDiamondHoe = config.get("Tainted Diamond Hoe", "ItemID", BaseID+29).getInt(BaseID+29);
        TaintedGoldHoe = config.get("Tainted Gold Hoe", "ItemID", BaseID+30).getInt(BaseID+30);
        TaintedIronHoe = config.get("Tainted Iron Hoe", "ItemID", BaseID+31).getInt(BaseID+31);
        TaintedStoneHoe = config.get("Tainted Stone Hoe", "ItemID", BaseID+32).getInt(BaseID+32);
        TaintedWoodHoe = config.get("Tainted Wood Hoe", "ItemID", BaseID+33).getInt(BaseID+33);

        BlessedDiamondSword = config.get("Blessed Diamond Sword", "ItemID", BaseID+34).getInt(BaseID+34);
        BlessedGoldSword = config.get("Blessed Gold Sword", "ItemID", BaseID+35).getInt(BaseID+35);
        BlessedIronSword = config.get("Blessed Iron Sword", "ItemID", BaseID+36).getInt(BaseID+36);
        BlessedStoneSword = config.get("Blessed Stone Sword", "ItemID", BaseID+37).getInt(BaseID+37);
        BlessedWoodSword = config.get("Blessed Wood Sword", "ItemID", BaseID+38).getInt(BaseID+38);
        BlessedDiamondPickaxe = config.get("Blessed Diamond Pickaxe", "ItemID", BaseID+39).getInt(BaseID+39);
        BlessedGoldPickaxe = config.get("Blessed Gold Pickaxe", "ItemID", BaseID+40).getInt(BaseID+40);
        BlessedIronPickaxe = config.get("Blessed Iron Pickaxe", "ItemID", BaseID+41).getInt(BaseID+41);
        BlessedStonePickaxe = config.get("Blessed Stone Pickaxe", "ItemID", BaseID+42).getInt(BaseID+42);
        BlessedWoodPickaxe = config.get("Blessed Wood Pickaxe", "ItemID", BaseID+43).getInt(BaseID+43);
        BlessedDiamondAxe = config.get("Blessed Diamond Axe", "ItemID", BaseID+44).getInt(BaseID+44);
        BlessedGoldAxe = config.get("Blessed Gold Axe", "ItemID", BaseID+45).getInt(BaseID+45);
        BlessedIronAxe = config.get("Blessed Iron Axe", "ItemID", BaseID+46).getInt(BaseID+46);
        BlessedStoneAxe = config.get("Blessed Stone Axe", "ItemID", BaseID+47).getInt(BaseID+47);
        BlessedWoodAxe = config.get("Blessed Wood Axe", "ItemID", BaseID+48).getInt(BaseID+48);
        BlessedDiamondShovel = config.get("Blessed Diamond Shovel", "ItemID", BaseID+49).getInt(BaseID+49);
        BlessedGoldShovel = config.get("Blessed Gold Shovel", "ItemID", BaseID+50).getInt(BaseID+50);
        BlessedIronShovel = config.get("Blessed Iron Shovel", "ItemID", BaseID+51).getInt(BaseID+51);
        BlessedStoneShovel = config.get("Blessed Stone Shovel", "ItemID", BaseID+52).getInt(BaseID+52);
        BlessedWoodShovel = config.get("Blessed Wood Shovel", "ItemID", BaseID+53).getInt(BaseID+53);
        BlessedDiamondHoe = config.get("Blessed Diamond Hoe", "ItemID", BaseID+54).getInt(BaseID+54);
        BlessedGoldHoe = config.get("Blessed Gold Hoe", "ItemID", BaseID+55).getInt(BaseID+55);
        BlessedIronHoe = config.get("Blessed Iron Hoe", "ItemID", BaseID+56).getInt(BaseID+56);
        BlessedStoneHoe = config.get("Blessed Stone Hoe", "ItemID", BaseID+57).getInt(BaseID+57);
        BlessedWoodHoe = config.get("Blessed Wood Hoe", "ItemID", BaseID+58).getInt(BaseID+58);

        EnderSword = config.get("Ender Sword", "ItemID", BaseID+59).getInt(BaseID+59);
        if (config.hasChanged()) {
            config.save();
        }
    }


}
